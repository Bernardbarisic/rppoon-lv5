﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD1_ZAD2
{
    class ShippingService
    {
        private double cargo;

        public ShippingService(double cargo) { this.cargo = cargo; }

        public double Price(double weight)
        {
            return cargo * weight;
        }

        public override string ToString()
        {
            return "Ukupna cijena dostave je:";
        }
    }
}
