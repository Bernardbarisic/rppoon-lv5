﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZAD1_ZAD2
{
    class Program
    {
        static void Main(string[] args)
        {
            Box gaming = new Box("Gaming equipment: ");
            List<IShipable> lists = new List<IShipable>();
            Product equipment = new Product("Keyboard, headset", 7, 9);
            ShippingService tax = new ShippingService(1.5);
            lists.Add(gaming);
            lists.Add(equipment);
            lists.Add(gaming);
            double weight = 0;
            foreach (IShipable list in lists)
            {

                weight += list.Weight;

            }

            Console.WriteLine(tax.ToString() + tax.Price(weight) + " hrk");
        }
    }
}
